import unittest
from unittest.mock import patch

import JPFlask
from JPFlask.forms import RegisterForm


class RegisterTestCase(unittest.TestCase):
    def setUp(self):
        app = JPFlask.create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_get_register_should_register_page(self):
        response = self.client.get('/user/register', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h1>', data)

    def test_register_no_email_should_register_page_with_required(self):
        with self.app.test_request_context('/'):
            form = RegisterForm(username='test', password='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h1>', data)
        self.assertIn('This field is required', data)

    def test_register_no_username_should_register_page_with_required(self):
        with self.app.test_request_context('/'):
            form = RegisterForm(email='test', password='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h1>', data)
        self.assertIn('This field is required', data)

    def test_register_no_password_should_register_page_with_required(self):
        with self.app.test_request_context('/'):
            form = RegisterForm(email='test', username='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h1>', data)
        self.assertIn('This field is required', data)

    def test_register_invalid_email_should_register_page_invalid_email(self):
        with self.app.test_request_context('/'):
            form = RegisterForm(email='test', username='test', password='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h1>', data)
        self.assertIn('invalid Email', data)

    @patch('JPFlask.user.models')
    def test_register_duplicate_should_register_page(self, dbmock):
        dbmock.User.objects().first.return_value = True
        with self.app.test_request_context('/'):
            form = RegisterForm(email='test@email.com',
                                username='test',
                                password='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>register</h', data)

    @patch('JPFlask.user.models')
    def test_register_valid_should_login_page(self, dbmock):
        dbmock.User.objects().first.return_value = False
        dbmock.User.save = True
        with self.app.test_request_context('/'):
            form = RegisterForm(email='test@email.com',
                                username='test',
                                password='test')
        response = self.client.post(
            '/user/register', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>login</h1>', data)

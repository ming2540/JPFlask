import unittest
from unittest.mock import patch

import JPFlask
from JPFlask.forms import PoemForm
import poem_analyze


class AlglornTestcase(unittest.TestCase):
    def setUp(self):
        app = JPFlask.create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_get_alglornrithm_index_should_ALGORNRITHM_page(self):
        response = self.client.get('/alglornrithm', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>This is Alglornrithm Module</h1>', data)

    def test_fill_form_with_a_a_a_a_a_should_return_ALGLORNRITHM_page(self):
        with self.app.test_request_context('/'):
            form = PoemForm(name='a', data1='a', data2='a',
                            data3='a', data4='a')
        response = self.client.post(
            '/alglornrithm', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        print(data)
        self.assertIn('<h1>This is Alglornrithm Module</h1>', data)
        self.assertIn('<h1>please input again</h1>', data)

    @patch('poem_analyze.alklorn')
    def test_fill_form_with_valid_should_return_result_page(self, pmock):
        with self.app.test_request_context('/'):
            form = PoemForm(name='aaaaa',
                            data1='แล้วสอนว่าอย่าไว้ใจมนุษย์',
                            data2='มันเเสนสุดลึกล้ำเหลือกำหนด',
                            data3='ถึงเถาวัลย์พันเกี่ยวที่เลี้ยวลด',
                            data4='ก็ไม่คดเหมือนหนึ่งในน้ำใจคน',)
        mock_poem = pmock()
        mock_poem.score = 50
        response = self.client.post(
            '/alglornrithm', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        print(data)
        self.assertIn('<h1>Result</h1>', data)

    def test_poem_analyze_with_standard_poem_should_get_93_point(self):
        expected_score = 95
        data = ['โอ้จำใจไกลนุชสุดสวาท',
                'จึงนิราศเรื่องรักเป็นอักษร',
                'ให้เห็นอกตกยากเมื่อจากจร',
                'ไปดงดอนแดนป่าพนาวัน']
        result = poem_analyze.alklorn(data)
        self.assertEquals(result['score'], expected_score)


if __name__ == '__main__':
    unittest.main()

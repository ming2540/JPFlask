import unittest
from unittest.mock import patch, MagicMock

import JPFlask
from JPFlask.forms import UserForm


class LoginTestCase(unittest.TestCase):
    def setUp(self):
        app = JPFlask.create_app()
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_get_login_should_login_page(self):
        response = self.client.get('/user/login', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>login</h1>', data)

    @patch('JPFlask.user.models')
    @patch('werkzeug.security.check_password_hash', return_value=False)
    def test_login_no_user_should_login_page(self, hmock, dbmock):
        dbmock.objects.get.return_value = {}
        with self.app.test_request_context('/'):
            form = UserForm(username='test', password='test')
        response = self.client.post(
            '/user/login', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>login</h1>', data)

    @patch('JPFlask.user.models')
    @patch('werkzeug.security.check_password_hash', return_value=True)
    @patch('flask_login.login_user', return_value=True)
    @patch('flask_login.utils._get_user')
    def test_login_should_hello_test(self, usermock, loginmock, hmock, dbmock):
        dbmock.objects.get.return_value = JPFlask.models.User(
            username='test', password='test')
        current_user = MagicMock()
        usermock.return_value = current_user
        with self.app.test_request_context('/'):
            form = UserForm(username='test', password='test')
        response = self.client.post(
            '/user/login', data=form.data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>This is JPFLask project!</h1>', data)
        self.assertIn('Hello', data)

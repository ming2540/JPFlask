import unittest
# from flask import url_for

import JPFlask


class JPFlaskTestCase(unittest.TestCase):
    def setUp(self):
        app = JPFlask.create_app()
        app.config['TESTING'] = True

        self.client = app.test_client()
        self.app = app

    def tearDown(self):
        pass

    def test_get_index_should_JPFlask_page(self):
        response = self.client.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        data = response.data.decode()
        self.assertIn('<h1>This is JPFLask project!</h1>', data)


if __name__ == '__main__':
    unittest.main()

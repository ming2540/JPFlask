from flask import Flask
from flask import render_template
from flask_login import LoginManager

# from . import site
from . import poem
from . import user
from . import models


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    login_manager = LoginManager()
    login_manager.init_app(app)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY="dev",
        MONGODB_DB="testdb"
    )

    @login_manager.user_loader
    def load_user(user_id):
        return models.User.objects(pk=user_id).first()

    models.init_db(app)
    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.update(test_config)

    @app.route("/")
    def index():
        return render_template('index.html')

    app.register_blueprint(poem.module)
    app.register_blueprint(user.module)

    return app

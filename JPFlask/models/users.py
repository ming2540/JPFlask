import mongoengine as me
import datetime
from flask_login import UserMixin, login_manager


class User(me.Document, UserMixin):
    meta = {'collection': 'users'}

    username = me.StringField()
    password = me.StringField()
    email = me.StringField()
    # is_active = me.BooleanField(default=True, required=True)
    created_date = me.DateTimeField(
        required=True, default=datetime.datetime.now)
    updated_date = me.DateTimeField(
        required=True, default=datetime.datetime.now)

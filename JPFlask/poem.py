from flask import (Blueprint, render_template)

from JPFlask.forms import PoemForm
import poem_analyze

module = Blueprint('alglornrithm', __name__, url_prefix='/alglornrithm')


@module.route('/', methods=['GET', 'POST'])
def index():
    form = PoemForm()

    if not form.validate_on_submit():
        return render_template(
            '/alglornrithm/index.html', form=form, input_error=True)

    data = [form.data1.data, form.data2.data, form.data3.data, form.data4.data]
    print(data)
    result = poem_analyze.alklorn(data)

    return render_template('/alglornrithm/result.html', result=result)

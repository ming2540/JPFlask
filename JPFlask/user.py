from flask import (Blueprint, render_template, redirect, url_for)

from JPFlask.forms import UserForm, RegisterForm
from JPFlask import models
# from werkzeug.security import generate_password_hash, check_password_hash
# from flask_login import login_user, login_required, logout_user
import datetime
import werkzeug
import flask_login

module = Blueprint('user', __name__, url_prefix='/user')


@module.route('/login', methods=['GET', 'POST'])
def login():
    form = UserForm()

    if not form.validate_on_submit():
        return render_template('/login.html', form=form, input_error=True)

    user = models.User.objects.get(username=form.username.data)
    if werkzeug.security.check_password_hash(
            user.password,
            form.password.data):
        flask_login.login_user(user)
        return redirect(url_for('index'))
    return render_template('/login.html', form=form)


@module.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if not form.validate_on_submit():
        return render_template('/register.html', form=form, input_error=True)
    if not models.User.objects(username=form.username.data).first():
        user = models.User(
            created_date=datetime.datetime.now(),
            updated_date=datetime.datetime.now(),
        )
        form.populate_obj(user)
        user.password = werkzeug.security.generate_password_hash(
            form.password.data)
        user.save()
        return redirect(url_for('user.login'))
    return render_template(
        '/register.html', form=form, duplicate_error=True)


@module.route('/logout', methods=['GET'])
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return redirect(url_for('user.login'))

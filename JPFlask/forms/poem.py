import flask_wtf
from wtforms import fields
from wtforms import validators


class PoemForm(flask_wtf.FlaskForm):
    name = fields.StringField()
    data1 = fields.StringField(validators=[validators.InputRequired(),
                                           validators.Length(min=3)])
    data2 = fields.StringField(validators=[validators.InputRequired(),
                                           validators.Length(min=3)])
    data3 = fields.StringField(validators=[validators.InputRequired(),
                                           validators.Length(min=3)])
    data4 = fields.StringField(validators=[validators.InputRequired(),
                                           validators.Length(min=3)])

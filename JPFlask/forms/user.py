from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators


class UserForm(FlaskForm):
    username = fields.StringField()
    password = fields.PasswordField()


class RegisterForm(FlaskForm):
    username = fields.StringField(
        validators=[
            validators.InputRequired()
        ]
    )
    email = fields.StringField(
        validators=[
            validators.InputRequired(),
            validators.Email(message='invalid Email')
        ]
    )
    password = fields.PasswordField(
        validators=[
            validators.InputRequired()
        ]
    )

// #include<iostream>
// #include<string>
// #include<vector>

// using namespace std;

// int main(){
//     int sts;
//     string input;
//     string get_command;

//     cin >> input;

//     get_command.append("thpronun -p ");
//     get_command.append(input);
//     get_command.append(" > output.txt");

//     vector<char> cstr(get_command.c_str(), get_command.c_str() + get_command.size() + 1);


//     sts = system(command);

//     return 0;
// }
#include <Python.h>
#include <bytesobject.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <cstring>
#include <cstdlib>


using namespace std;

const char vowels[] = {
    'a','e','i','o','u'
};
#define VOWELS_COUNT 5

typedef struct {
    int score=0;
    int suparb=1;
    vector<string> klorn_result;
    vector<int> out_rhyme;
    vector<int> in_rhyme;
    vector<int> tone;
    vector<int> alpha_rhyme;
    vector<int> between_bot_rhyme;
}Result;


vector<string> wuk_to_payang(string wuk){
    vector<string> payang;
    istringstream iss(wuk);
    string temp = "";

    while(iss >> temp){
        payang.push_back(temp);
    }
    return payang;
}

vector<vector<string>> text_to_payangs(const char* cmd) {
    int count=0;
    array<char, 128> buffer;
    vector<string> wuks;
    vector<vector<string>> result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        if(count>0)
            wuks.push_back( buffer.data() );
        count++;
    }
    for(auto sub_result : wuks)
        result.push_back(wuk_to_payang(sub_result));

    return result;
}

bool is_rhyme(string payang1, string payang2){
    size_t rhyme1=payang1.size()-1;
    size_t rhyme2=payang2.size()-1;

    for(int i=0; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(rhyme1 > payang1.find(vowels[i]))
            rhyme1 = payang1.find(vowels[i]);
        if(rhyme2 > payang2.find(vowels[i]))
            rhyme2 = payang2.find(vowels[i]);
    }
    string temp1 = payang1.substr(rhyme1,payang1.size() - (rhyme1+1) );
    string temp2 = payang2.substr(rhyme2,payang2.size() - (rhyme2+1) );

    // cout << temp1 << " and " << temp2 << endl;
    return (temp1.compare(temp2)==0)? true : false;
}

bool is_rhyme_in_alphabet(string payang1, string payang2){
    size_t ryhme1=payang1.size()-1;
    size_t ryhme2=payang2.size()-1;

    // ryhme1 = payang1.find(vowels[0]);
    // ryhme2 = payang1.find(vowels[0]);
    for(int i=0; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(ryhme1 > payang1.find(vowels[i]))
            ryhme1 = payang1.find(vowels[i]);
        if(ryhme2 > payang2.find(vowels[i]))
            ryhme2 = payang2.find(vowels[i]);
    }
    string temp1 = payang1.substr(0,ryhme1);
    string temp2 = payang2.substr(0,ryhme2);

    // cout << temp1 << " vs " << temp2;
    // if(temp1.compare(temp2)==0)cout << "YES";
    // cout << endl;

    return (temp1.compare(temp2)==0)? true : false;
}

int check_rhyme_between_wuk(vector<string> wuk1, vector<string> wuk2, int type_of_wuk){
    int result = 0;
    //0 means bad, 1 means good
    // cout << wuk1.at(wuk1.size()-1) << " vs " <<  wuk2.at(2) << "/" << wuk2.at(4) << endl;
    if(wuk1.size()==0 || wuk2.size()==0)return 0;


    switch(type_of_wuk){
        //à¸ªà¸”à¸±à¸š-à¸£à¸±à¸š
        case 1: //same as 3
        //à¸£à¸­à¸‡-à¸ªà¹ˆà¸‡
        case 3: if( is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(2)) || is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(4)) )
                result = 1;
                break;
        //à¸£à¸±à¸š-à¸£à¸­à¸‡
        case 2: //same as 4
        //à¸ªà¹ˆà¸‡-à¸ªà¹ˆà¸‡
        case 4: if( is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(wuk2.size()-1)) ){
                // cout 
                result = 1;
                }       
                break;
        default: break;
    }
    

    return result;
}   

int check_rhyme_alphabet_in_wuk(vector<string> wuk){
    int result=0;
    if(wuk.size()==0)return 0;

    for(size_t i=0; i<wuk.size()-1; i++){
        // for(size_t j=i+1; j<wuk.size(); j++){
        //     if(is_rhyme_in_alphabet(wuk.at(i),wuk.at(j)))
        //         result++;
        // }
        size_t j=i+1;
        while(j<wuk.size()  && is_rhyme_in_alphabet(wuk.at(i), wuk.at(j))){
            // cout << wuk.at(i) << " and " << wuk.at(j) << endl;
            result++;
            j++;
        }
        i=j-1;
    }
    return result;
}

int check_rhyme_in_wuk(vector<string> wuk){
    int result = 0;
    // 0 means ok, 1 means good, 2 means very good
    if(wuk.size()==0)return 0;

    if( is_rhyme(wuk.at(2), wuk.at(3)) )
        result+=1;
    if( is_rhyme(wuk.at(4), wuk.at(5)) || is_rhyme(wuk.at(4), wuk.at(6)) )
        result+=1;
    
    return result;
}

int check_tone(vector<string> wuk, int type_of_wuk){
    if(wuk.size()==0)return 0;

    string check_wuk = wuk.at(wuk.size()-1);
    char tone = check_wuk.at(check_wuk.size()-1);
    int result = 0;
    // 0 means bad, 1 means ok, 2 means good
    
    switch(type_of_wuk){
        //à¸ªà¸”à¸±à¸š
        case 1: if(tone == '0')
                    result = 1;
                else 
                    result = 2;
                break;
        //à¸£à¸±à¸š
        case 2: if(tone == '0' || tone == '3')
                    result = 0;
                else if(tone == '4')
                    result = 2;
                else 
                    result = 1;
                break;
        //à¸£à¸­à¸‡
        case 3: // same as case 4
        //à¸ªà¹ˆà¸‡
        case 4: if(tone == '0' || tone == '3')
                    result = 2;
                else 
                    result = 0;
                break;
        default: break;
    }

    return result;
}


Result alklornrithm( vector<vector<vector<string>>>klorn,vector<string> buffer){
    Result result;
    int score=0;
    int best_position[4];
    int temp;

    vector<vector<string>> sadab;
    vector<vector<string>> rub;
    vector<vector<string>> rong;
    vector<vector<string>> song;
    vector<string> last_song;
    vector<vector<string>> tbuffer;
    string command = "thpronun -t ";

    // cout << klorn.size() << endl;
    if(klorn.size()%4!=0)
        throw logic_error("not in 4 wuks");

    cout << endl;
    for(size_t klorn_index=0; klorn_index != klorn.size(); klorn_index+=4){
        score = 0;
        for(size_t i=0; i<4; i++){
            switch(i){
                case 0: sadab.assign(klorn.at(klorn_index+i).begin(),klorn.at(klorn_index+i).end());
                        break;
                case 1: rub.assign(klorn.at(klorn_index+i).begin(),klorn.at(klorn_index+i).end());
                        break;
                case 2: rong.assign(klorn.at(klorn_index+i).begin(),klorn.at(klorn_index+i).end());
                        break;
                case 3: song.assign(klorn.at(klorn_index+i).begin(),klorn.at(klorn_index+i).end());
                        break;
            }
        }
        
        for(size_t i=0; i!=sadab.size(); ++i){
            for(size_t j=0; j!=rub.size(); ++j){
                for(size_t k=0; k!=rong.size(); ++k){
                    for(size_t l=0; l!=song.size(); ++l){
                        // temp = check_rhyme_alphabet_in_wuk(sadab.at(i));
                        temp = check_rhyme_between_wuk(sadab.at(i),rub.at(j),1)*15 +
                            check_rhyme_between_wuk(rub.at(j),rong.at(k), 2)*15 +
                            check_rhyme_between_wuk(rong.at(k),song.at(l), 3)*15 +
                            check_rhyme_in_wuk(sadab.at(i))*3 +
                            check_rhyme_in_wuk(rub.at(j))*3 +
                            check_rhyme_in_wuk(rong.at(k))*3 +
                            check_rhyme_in_wuk(song.at(l))*3 +
                            check_tone(sadab.at(i),1)*3 +
                            check_tone(rub.at(j),2)*3 +
                            check_tone(rong.at(k),3)*3 +
                            check_tone(song.at(l),4)*3 +
                            check_rhyme_alphabet_in_wuk(sadab.at(i)) +
                            check_rhyme_alphabet_in_wuk(rub.at(j)) +
                            check_rhyme_alphabet_in_wuk(rong.at(k)) +
                            check_rhyme_alphabet_in_wuk(song.at(l))
                            ;
                        if(klorn_index > 0)
                            temp += check_rhyme_between_wuk(last_song,rub.at(j),4)*5;

                        if(sadab.at(i).size()!=8)
                            temp-= abs((int)(sadab.at(i).size()-8)*2);
                        if(rub.at(j).size()!=8)
                            temp-= abs((int)(rub.at(j).size()-8)*2);
                        if(rong.at(k).size()!=8)
                            temp-= abs((int)(rong.at(k).size()-8)*2);
                        if(song.at(l).size()!=8)
                            temp-= abs((int)(song.at(l).size()-8)*2);

                        if(score < temp){
                            score = temp;
                            best_position[0]=i;
                            best_position[1]=j;
                            best_position[2]=k;
                            best_position[3]=l;
                        }
                    }            
                }
            }
        }

        result.score += score;

        for(size_t i=0; i<4; i++){
            command.append(buffer.at(klorn_index+i));
            tbuffer = text_to_payangs( command.c_str() ) ;
            command.erase(command.begin()+12,command.end() );
            for(auto sub_payang : tbuffer.at(best_position[i])){
                result.klorn_result.push_back(sub_payang);
            }
            tbuffer.clear();
        }
        // string temptemp="";
        // for(auto sub : sadab.at(best_position[0])){
        //     temptemp+=sub;
        //     temptemp+=" ";
        // }
        // result.klorn_result.push_back(temptemp);
        // temptemp.erase();
        // for(auto sub : rub.at(best_position[1])){
        //     temptemp+=sub;
        //     temptemp+=" ";
        // }
        // result.klorn_result.push_back(temptemp);
        // temptemp.erase();
        // for(auto sub : rong.at(best_position[2])){
        //     temptemp+=sub;
        //     temptemp+=" ";
        // }
        // result.klorn_result.push_back(temptemp);
        // temptemp.erase();
        // for(auto sub : song.at(best_position[3])){
        //     temptemp+=sub;
        //     temptemp+=" ";
        // }
        // result.klorn_result.push_back(temptemp);
        // temptemp.erase();


        result.out_rhyme.push_back( check_rhyme_between_wuk(sadab.at(best_position[0]),rub.at(best_position[1]),1) );
        result.out_rhyme.push_back( check_rhyme_between_wuk(rub.at(best_position[1]),rong.at(best_position[2]), 2) );
        result.out_rhyme.push_back( check_rhyme_between_wuk(rong.at(best_position[2]),song.at(best_position[3]), 3) );

        result.in_rhyme.push_back( check_rhyme_in_wuk(sadab.at(best_position[0])) );  
        result.in_rhyme.push_back( check_rhyme_in_wuk(rub.at(best_position[1])) );
        result.in_rhyme.push_back( check_rhyme_in_wuk(rong.at(best_position[2])) );
        result.in_rhyme.push_back( check_rhyme_in_wuk(song.at(best_position[3])) );

        result.tone.push_back( check_tone(sadab.at(best_position[0]),1) );
        result.tone.push_back( check_tone(rub.at(best_position[1]),2) );
        result.tone.push_back( check_tone(rong.at(best_position[2]),3) );
        result.tone.push_back( check_tone(song.at(best_position[3]),4) );

        result.alpha_rhyme.push_back( check_rhyme_alphabet_in_wuk(sadab.at(best_position[0])) );
        result.alpha_rhyme.push_back( check_rhyme_alphabet_in_wuk(rub.at(best_position[1])) );
        result.alpha_rhyme.push_back( check_rhyme_alphabet_in_wuk(rong.at(best_position[2])) );
        result.alpha_rhyme.push_back( check_rhyme_alphabet_in_wuk(song.at(best_position[3])) );

        if( sadab.at(best_position[0]).size() !=8 ||
            rub.at(best_position[1]).size() !=8 ||
            rong.at(best_position[2]).size() !=8 ||
            song.at(best_position[3]).size() !=8
         )
            result.suparb = 0;

        if(klorn_index>0)
            result.between_bot_rhyme.push_back( check_rhyme_between_wuk(last_song,rub.at(best_position[1]),4) );
        
        last_song.assign(song.at(best_position[3]).begin(), song.at(best_position[3]).end());
    }

    // for(auto sub : result.klorn_result)
    //     cout << sub << endl;
    // cout << "out:";
    // for(auto sub : result.out_rhyme)
    //     cout << sub << ",";
    // cout << endl;
    // cout << "in:";
    // for(auto sub : result.in_rhyme)
    //     cout << sub << ",";
    // cout << endl;
    // cout << "tone:";
    // for(auto sub : result.tone)
    //     cout << sub << ",";
    // cout << endl;
    // cout << "alpha:";
    // for(auto sub : result.alpha_rhyme)
    //     cout << sub << ",";
    // cout << endl;
    // cout << "bot:";
    // for(auto sub : result.between_bot_rhyme)
    //     cout << sub << ",";
    // cout << endl;

    result.score/=(klorn.size()/4);
    if(result.score > 100)result.score=100;

    // cout << "result = " <<  result.score << endl;   

    return result;
}


Result call_alklorn(vector<string> buffer){
    vector<vector<vector<string>>> klorn;
    string command = "thpronun -p ";
    
    for(int i=0; i < buffer.size(); i++){
        command.append(buffer.at(i));
        klorn.push_back(text_to_payangs( command.c_str() ) );
        command.erase(command.begin()+12,command.end() );
    }

    return alklornrithm(klorn,buffer);
}

static PyObject *SpamError;

PyObject *vectorToList_String(vector<string> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyUnicode_FromString(dataList.at(i).c_str());
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

PyObject *vectorToList_Int(vector<int> dataList){
    PyObject *pyList = PyList_New(dataList.size());
    if(!pyList)
        throw logic_error("unable to allocate memory for list");
    for(size_t i=0; i!=dataList.size(); i++){
        PyObject *data = PyLong_FromLong(dataList.at(i));
        if(!data)
            throw logic_error("unable to allocate memmory for string");
        PyList_SetItem(pyList, i, data);
    }

    return pyList;
}

static PyObject *
alklorn(PyObject *self, PyObject *args){
    Result result;
    // PyObject *result;
    vector<string> get_input;
    string buffer;
    PyObject *obj;

    if(!PyArg_ParseTuple(args, "O", &obj))
        return NULL;
    
    if(!PyList_Check(obj))
        throw logic_error("not a list");
    for(Py_ssize_t i=0; i<PyList_Size(obj); i++){
        PyObject *iter = PyList_GetItem(obj, i);
        PyObject *str = PyUnicode_AsEncodedString(iter, "utf-8", "~E~");
        
        get_input.push_back( PyBytes_AS_STRING(str) );
    }

    result = call_alklorn(get_input);
    // cout << is_rhyme_in_alphabet("pheym2", "phahy0");

    // return Py_BuildValue("s","finished");
    return Py_BuildValue("{s : i, s : i, s : O, s : O, s : O, s : O, s : O, s:O}","score"
                                                                    ,result.score
                                                                    ,"suparb"
                                                                    ,result.suparb
                                                                    ,"klorn_result"
                                                                    ,vectorToList_String(result.klorn_result)
                                                                    ,"out_rhyme"
                                                                    ,vectorToList_Int(result.out_rhyme)
                                                                    ,"in_rhyme"
                                                                    ,vectorToList_Int(result.in_rhyme)
                                                                    ,"tone"
                                                                    ,vectorToList_Int(result.tone)
                                                                    ,"alpha_rhyme"
                                                                    ,vectorToList_Int(result.alpha_rhyme)
                                                                    ,"between_bot_rhyme"
                                                                    ,vectorToList_Int(result.between_bot_rhyme)
                                                                    );
}

static PyMethodDef poem_analyze_method[]={
    {"alklorn", alklorn, METH_VARARGS,
     "Execute a shell command."},
    {NULL, NULL , 0, NULL}//sentinel
};

static struct PyModuleDef poem_analyze_module{
    PyModuleDef_HEAD_INIT,
    "poem_analyze",//name of module
    NULL, // doc
    -1,
    poem_analyze_method
};

PyMODINIT_FUNC
PyInit_poem_analyze(void){
    return PyModule_Create(&poem_analyze_module);
}

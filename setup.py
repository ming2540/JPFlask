import io

from setuptools import find_packages
from setuptools import setup
from setuptools import Extension

poem_analyze_module = Extension('poem_analyze', sources=['alglornrithm.cpp'])

requires = []
with open('requirements.txt', 'r') as fp:
    requires = [t.strip() for t in fp.read().split('\n') \
                if len(t.strip()) > 0]


setup(
    name='JPFlask',
    version='0.0.1',
    url='',
    license='BSD',
    maintainer='',
    maintainer_email='',
    description='Basic web for CI/CD workflow.',
    long_description='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    extras_require={'test': ['nosetests', 'coverage']},
    ext_modules=[poem_analyze_module]
)
